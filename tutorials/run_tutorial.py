#!/usr/bin/env python
# coding: utf-8

# # Run tutorial
#
# This tutorial is a guide to compute finite-element solutions for
# CSEM setups with custEM. In the current state, we considered a small
# model which can be run without MPI in serial. It is similar to example '#'1
# in the examples directory. In addition, a plate anomaly was incorporated
# in the model and the transmitter is a rectangular loop now. Please note that
# the documentation of custEM is still in development as is this tutorial.

# In[ ]:


# imports

import dolfin as df
import numpy as np
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen import meshgen_utils as mu
from custEM.core import MOD


# ### On-the-fly mesh generation

# In[ ]:


# Alternatively to calling a mesh generation script like the *meshgen_tuorial*
# or the *topo_mesh_tutorial* before running a simulation script,
# we can also create the mesh on-thy-fly in the simulation script, which is
# called from the command line, e.g. *mpirun -n 4 python -u run_tutorial.py*
# #############################################################################

if df.MPI.rank(df.MPI.comm_world) == 0:
        # create world with default dimensions of 1e4 m in each direction
        O = BlankWorld(name='test_mesh',
                       preserve_edges=True)

        # specify 10 km x-directed surface observation line
        rx1 = mu.line_x(start=-5e3, stop=5e3, n_segs=100)
        rx1_tri = mu.refine_rx(rx1, 5., 30)

        # specify 2x2 km airborne observation grid
        n = 21
        vec = np.linspace(-1000., 1000., n)
        rx2 = np.zeros((n**2, 3))
        counter = 0
        for x in vec:
            for y in vec:
                rx2[counter, 0] = x
                rx2[counter, 1] = y
                counter += 1
        rx2[:, 2] = 50.                         # set height of points to 50 m
        rx2_tri = mu.refine_rx(rx2, 10., 30)

        O.build_surface(
            insert_line_tx=[mu.line_y(start=-5e2, stop=5e2,
                                      n_segs=20)],  # add first line Tx
            insert_loop_tx=[mu.loop_r(start=[-5e2, -5e2], stop=[5e2, 5e2],
                                      n_segs=40)],  # add second loop Tx
            insert_paths=rx1_tri)                   # add surface Rx positions

        # build halfspace mesh and extend 2D surface mesh to 3D world
        O.build_layered_earth_mesh(n_layers=3,
                                   layer_depths=[-300., -1000.])

        # add airborne Rx
        O.add_paths(rx2_tri)                    # add refinement triangles
        O.add_rx(rx1)                           # Rx info for parameter file
        O.add_rx(rx2)                           # Rx info for parameter file

        # Add plate anomaly
        O.add_plate(500., 500., 100.,           # length, width, height
                    [100., 0., -700.],          # origin (center) of plate
                    45., 117.,                  # dip, dip azimuth
                    cell_size=1e4)              # max. cell volume constraint

        # Append boundary mesh if required
        # O.extend_world(5., 5., 5.)

        # call TetGen
        O.call_tetgen(tet_param='-pq1.4aA')
else:
    pass


# ### Initialize MOD instance

# In[ ]:


# Specify approach, meanwhile CSEM, TEM and MT approaches possible
# #############################################################################

# for controlled-source frequency-domain (CSEM)
approach = 'E_t'              # total electric field formulation
# approach = 'E_s'            # secondary electric field formulation
# approach = 'H_s'            # secondary magnetic field formulation
# or one of the potential approaches 'Am_t', 'Am_s', 'An_t', 'An_s', 'Fm_s'

# for controlled-source time-domain (TEM)
# approach = 'E_IE'           # implicit Euler time-stepping method
# approach = 'E_FT'           # fourier-transform based method
# approach = 'E_RA'           # rational Arnoldi method

# for natural-source frequency-domain (MT)
# approach = 'MT'             # total electroc field formulation
# the following option is not implemented yet, but will be probably
# implemented in future as an alternative to the total-field formulation
# approach = 'MT_s'           # secondary electric field formulation

M = MOD('test_mod',           # mod name (required)
        'test_mesh',          # mesh name (required)
        approach,             # approach name (required)
        p=1,                  # polynomial order
        overwrite_mesh=True,  # automatically overwrite existing mesh if a
                              # a newer version was created
        overwrite_results=True,  # overwrite existing results for this mesh
        serial_ordering=True,    # use serial ordering (for solver robustness)
        debug_level=10,          # define log level, 10 = info
        # load_existing=False,   # import existing results of this model,
        #                        # requires *overwrite_results=False*
        )


# ### Set model parameters
#
# - A single frequencies is specified as *frequency* or *omega*,
# - Multiple frequencies are specified as *frequencies* or *omegas*
# - The conductivity distribution needs to be set in the order layers
#   or anoamlies were added to the mesh, e.g., this example requires a list of
#   4 entries for *sigma_ground*
# - only secondary-field approaches require specification of *sigma_0*, here,
#   we need 4 background resistivity values for *sigma_0*
#   to calculate *delta_sigma* on the fly
# - sigma_air is 1e-8 by default and only relevant for total field approaches
#   we found that a contrastto the subsurface resistivities of 4 orders of
#   magnitude is totally sufficient to obtain accurate results

# In[ ]:


# update model parameters
# #############################################################################

M.MP.update_model_parameters(
        frequencies=[10, 100, 1000],     # multiple frequencies (Hz)
        # omega=1e1,                     # e.g., only one angular frequency
        # current=1.,                    # set equal current for all Tx
        # currents=[1., -5.],            # unequal source currents for each Tx
        sigma_ground=[1e-3, 1e-2,        # layer conductivities (top to bot.)
                      1e-4, 1e-1],       # + one value for anomaly
        # sigma_0=[1e-3, 1e-2,           # background conducitivites, last one
        #          1e-4, 1e-2],          # for anomaly (in second layer)
        # sigma_air=1e-8,                # airspace conductivity
        )

# - anisotropic parameters are specified by 3 (VTI) or 6 (general, upper
#   triangle matrix of parameter tensor) values, e.g., to make the anomaly in
#   our example general anistropic in an VTI anisotropic background layer, use

# sigma_ground=[1e-3,
#               [2e-2, 2e-2, 1e-1],
#               1e-4,
#               [1e-1, 2e-1, 3e-1, 4e-1, 5e-1, 6e-1]
#               ]   # note the general anisotropic values here are nonsense

# - user can also specifiy values for *mu_r*, *eps_r* in the same manner
# - users can also specify the IP parameters *ip_m*, *ip_c*, *ip_tau*, but
#   this works only with isotropic conductivities for now (Cole-Cole model)


# ### Build var form and solve main problem

# In[ ]:


# set up variational formulation
# #############################################################################

# For the CSEM approaches, usually no additional keyword
# arguments are required, only for the full formulation or IP modeling

M.FE.build_var_form(# ip=True,                  # enable ip modeling
                    # quasi_static=True         # enable full formulation
                    )

# time-domain approaches require specifications of the observation times,
# either provide an explicit *times* vector or let custEM choose appropriate
# *times* automatically based on the ranges (recommended for most applications)

# the following is only relevant for time-domain approaches
# M.FE.build_var_form(n_log_steps=41,           # number of log time steps
#                     log_t_min=-4,             # log-start of times
#                     log_t_may=0,              # log-end of times
#                     n_lin_steps=30,           # for IE approach only
#                     )


# In[ ]:


# solve main problem and convert or export results
# #############################################################################

M.solve_main_problem(# convert_to_H=False,      # turn off H-field conversion
                     # export_pvd=False,        # turn off export for Paraview
                     # auto_interpolate=False   # enable auto-interpolation
                     #                          # (only numpy array export)
                     )


# ### Interpolation

# In[ ]:


# create interpolation meshes and interpolate on them
# #############################################################################

M.IB.create_line_mesh(
        'x',                        # x-directed line
        start=-5000.,               # start
        stop=5000.,                 # stop
        n_segs=100,                 # number of segments on line
        z=-0.1,                     # go a bit into the earth
        line_name='surf'            # set custom line name,
        )

M.IB.create_slice_mesh(
        'z',                        # z-normal slice
        dim=2e3,                    # total dimension,
        n_segs=20,                  # numeber of segments in each direction
        # dim=[2e3, 2e3],           # use two values for defining a rectangle
        # n_segs=[20, 20],          # for different n_segs in each direction
        slice_name='air',           # set custom slice name
        )

# lines in a specific direction get the ending *line_x/y/z*
# slices in a specific direction get the ending *slice_x/y/z*

M.IB.interpolate(
        ['surf_line_x', 'air_slice_z'],   # interpolation meshes
        # quantities=['E_t', 'H_t']       # specify fields to interpolate
        )

# in secondary field approaches and if interested in interpolated secondary
# fields use *quantities=['E_t, 'H_t', 'E_s', 'H_s']*


# alternatively, users can use "auto_interpolate" to specify on all rx paths
# included during the mesh generation. In this case, no Paraview-files are
# exported and solutions for all Tx are written to a single numpy file

