#!/usr/bin/env python
# coding: utf-8

# # Mesh generation with topography data
#
# This tutorial is a guide to create topography meshes for calculations
# whit custEM. It is possible to either describe a synthetic topography as
# f(x, y) or to incorporate real DEM (digital elevation model) data.
# Synthetic topographies can be definied in *the synthetic_definitions.py*
# or a custom file or directly in this script as will be explained following.
#
# As real DEM data files are quite large, we present the usage and structure
# by exporting synthetic topography data to both supported file formats
# and use these data to simulate real-world topography interpolation.
#
# In the following, we will handle two seperate cases, named **I** and **II**,
# to show different options or possibilites.

# ### General information for DEM files
#
# - supported file formats are either *xyz* or *asc* files
# - for now, the DEM data points must be located on a regular grid
# - the files must be located in the topography directory **t_dir**
# - *xyz* files contain x-, y-, z- values for each single data point.
#   The positions must be sorted either **ascending** in x-y coordinate
#   order or vice versy, in y-x order
# - *asc* files contain a header with lower left corner and grid spacing,
#   followed by a matrix of z-values arranged corresponding to the grid
# - for more information, generate the example files as coded below and
#   have look at the them

# ### Import modules, utility functions or synthetic definitions

# In[ ]:


from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
import numpy as np

# import synthetic example-topography description, here valley.
# #############################################################################
from custEM.misc.synthetic_definitions import valley

t_dir = '.'                     # directory for topography files


# ### case I - Create example *xyz* file
#
# Here we use the *valley* function from *synthetic_definitions*

# In[ ]:


v = np.linspace(0., 2e4, 801)   # vector with 25 m spacing
xy = np.zeros((len(v)**2, 2))   # empty 2D array for all nodes in grid
a = 0
for j in range(len(v)):         # assign coordinates for rows
    for k in range(len(v)):     # assign coordinates for columns
        xy[a, 0] = v[j]
        xy[a, 1] = v[k]
        a += 1

# use imported synthetic topo-function to calculate z-values
z_data = valley(xy[:, 0], xy[:, 1])
valley_data = np.hstack((xy, z_data.reshape(-1, 1)))

# write xyz file
mu.write_synth_topo_to_xyz(t_dir, 'valley_DEM', valley_data)


# ### case II - Create example *asc* file
#
# for this example, we will use a locally definied synthetic function

# In[ ]:


# Define topography function
# #############################################################################
np.random.seed(42)  # set seed for reproducibility

def slope1(x, y, h=300., z=200.):
    return(1e-4 * h * x - 1e-4 * h * y + z)

v = np.linspace(-2e4, 2e4, 401)  # vector with 100 m spacing
xy = np.zeros((len(v)**2, 2))    # empty 2D array for all nodes in grid
a = 0
for j in range(len(v)):          # assign coordinates for rows
    for k in range(len(v)):      # assign coordinates for columns
        xy[a, 0] = v[j]
        xy[a, 1] = v[k]
        a += 1

z_data = slope1(xy[:, 0], xy[:, 1])           # some basic slope
z_data += np.random.rand(len(z_data)) * 100.  # add the "hills"

# write asc file and assume we have some UTM 32 N coordinates, that's why
# we shift our just generated 40 x 40 km outcrop of topo data with help of
# *x_min* and *y_min* to the assumed "real" coordinate world
mu.write_synth_topo_to_asc(t_dir,
                           'hilly_DEM',
                           z_data.reshape(len(v), len(v)),
                           spacing=100.,      # header infor
                           x_min=703000,      # lower left x coordinate
                           y_min=5602000)     # lower left y coordinate


# ### Create computational domains for cases I and II
#
# #### Incorporating topography
#
# It is possible to define topography (otherwiese topo=None) either with
# functions or DEM files. The toolbox will automatically calculate z-values
# for corresponding nodes if a function is used or interpolate
# z-values from the DEM using the scipy *regulargridinterpolator* on the fly.
#
# The examples aim to show the usage of the following important arguments:
#
# 1. centering: used to shift the computational domain Omega relative to the
#    center of the coordinate frame. This is in particular useful to shift, for
#    instance, a 20 x 20 km outcrop of DEM data to the modeling domain.
#
# 2. easting_shift & northing_shift: shift Omega relative to the extend of the
#    DEM in x- and y- direction by these values. This can be used, for instance,
#    to shift a start- or end-point of the transmitter in the model domain to
#    the coordiantes measured in the field and hence, simplify the models setup.
#
# 3. rotation: rotate clockwise, starting with positive x-axis direction, the
#    coordinate frame in the field relative to the model domain. This is useful
#    if a survey setup with parallel and perpedicular lines should correspond
#    to x- and y-axes in the model domain

# In[ ]:


# Incorporate topography with *asc* file
# #############################################################################

M1 = BlankWorld(                     # create blank 3D domain Omega
     name='hilly_mesh',              # name of the output mesh
     m_dir='./meshes',               # main mesh directory
     t_dir=t_dir,                    # topo dir., **p_dir**+ '/topo'
     # x_dim=[-1e4, 1e4],            # x-dimension [m] of the mesh
     # y_dim=[-1e4, 1e4],            # y-dimension [m] of the mesh
     # y_dim=[-1e4, 1e4],            # z-dimension [m] of the mesh
     backup_script = False,          # store mesh generation script as backup
     #
     # keyword arguments regarding building the 2D surface mesh.
     #
     inner_area='box',           # refinement of area at surface
     inner_area_size=[2e3, 1e3],     # size of this area
     # inner_area_shift=[0., 0.],    # shift in x- and y-dir of area
     inner_area_cell_size=1e3,       # max. triangle-size (MA) in area
     outer_area_cell_size=1e5,       # MA outside of this area

     # keyword arguments regarding incorporation of topography
     #
     topo='hilly_DEM.asc',           # topography (from DEM or synthetic),
     # # # # # # # # # # # # # # # # #                      e.g. topo=valley
     # subsurface_topos=None,        # subsurface interface topo (DEM, synth.)
     easting_shift=-723000,          # shift DEM east. relative to model domain
     northing_shift=-5622000,        # shift DEM north. relative to model domain
     # centering=False,              # center DEM to model domain
     rotation=127.,                  # rotate DEM around relative to Omega in °
     )

# Incorporate topography with *xyz* file
# #############################################################################

M2 = BlankWorld(                     # blank 3D domain
     name='valley_mesh_from_xyz',    # name of the output mesh
     m_dir='./meshes',               # main mesh directory
     t_dir=t_dir,                    # topo dir., **p_dir**+ '/topo'
     backup_script = False,          # store mesh generation script as backup
     #
     # keyword arguments regarding building the 2D surface mesh.
     #
     # inner_area='ellipse',         # refinement of area at surface
     # inner_area_size=[2e3, 1e3],   # size of this area
     # inner_area_shift=[0., 0.],    # shift in x- and y-dir of area
     # inner_area_cell_size=1e3,     # max. triangle-size (MA) in area
     # outer_area_cell_size=1e5,     # MA outside of this area

     # keyword arguments regarding incorporation of topography
     #
     topo='valley_DEM.xyz',          # topography (from DEM or synthetic),
     # # # # # # # # # # # # # # # # #                      e.g. topo=valley
     # subsurface_topos=None,        # subsurface interface topo (DEM, synth.)
     # easting_shift=-723000,        # shift DEM east. relative to model domain
     # northing_shift=-5622000,      # shift DEM north. relative to model domain
     centering=True,                 # center DEM to model domain
     # rotation=None,                # rotate DEM realative to model domain
     )

# Incorporate topography with f(x,y) definition with a function
# #############################################################################

M3 = BlankWorld(                     # blank 3D domain
     name='valley_mesh_from_func',   # name of the output mesh
     m_dir='./meshes',               # main mesh directory
     t_dir=t_dir,                    # topo dir., **p_dir**+ '/topo'
     backup_script = False,          # store mesh generation script as backup
     #
     # keyword arguments regarding building the 2D surface mesh.
     #
     # inner_area='ellipse',         # refinement of area at surface
     # inner_area_size=[2e3, 1e3],   # size of this area
     # inner_area_shift=[0., 0.],    # shift in x- and y-dir of area
     # inner_area_cell_size=1e3,     # max. triangle-size (MA) in area
     # outer_area_cell_size=1e5,     # MA outside of this area

     # keyword arguments regarding incorporation of topography
     #
     topo=valley,                    # topography (from DEM or synthetic),
     # # # # # # # # # # # # # # # # #                      e.g. topo=valley
     # subsurface_topos=None,        # subsurface interface topo (DEM, synth.)
     # easting_shift=-723000,        # shift DEM east. relative to model domain
     # northing_shift=-5622000,      # shift DEM north. relative to model domain
     # centering=False,              # center DEM to model domain
     # rotation=None,                # rotate DEM realative to model domain
     )


# ### Create surface mesh

# In[ ]:


# Build the 2D surface mesh and incorporate transmitter or observation lines.
# #############################################################################

M1.build_surface(
    # insert_lines=[mu.line_x(-5e3, 5e3, 1000)],  # add observation line
    insert_loop_tx=[mu.loop_r(start=[-5e2, -5e2],
                              stop=[5e2, 5e2], n_segs=100)],  # add Tx loop_r
    )

M2.build_surface(
    insert_line_tx=[mu.line_x(-1e3, 1e3, 1000)],   # add line Tx
    )
M3.build_surface(
    insert_line_tx=[mu.line_x(-1e3, 1e3, 1000)],   # add line Tx
    )


# ### Build 3D world based on 2D surface mesh
#
# - Build either a fullspace, halfspace or layered-earth mesh

# In[ ]:


# The most simple world is a fullspace, in this case the **build_surface**
# method does not need to be executed or is ignored, respectively.
# #############################################################################

# M1.build_fullspace_mesh()


# In[ ]:


# Build a halfspace model or halfspace-like if topography is not *None*.
# #############################################################################

M1.build_halfspace_mesh()


# In[ ]:


# Build a layered-earth mesh, topography can be set for subsurface layers
# when initializing the **BlankWorld** or in this function call as keyword arg.
# If subsurface topo is used, *layer_depths* will be ignored.
# #############################################################################

M2.build_layered_earth_mesh(n_layers=2,                   # number of layers
                            layer_depths=[-1200.],         # n-1 layer depths
                            )
M3.build_layered_earth_mesh(n_layers=2,                   # number of layers
                            layer_depths=[-1200.],         # n-1 layer depths
                            )


# ### Add anomalies to the subsurface
#
# - Anomalies are not allowed to intersect layers (for now)!
#

# In[ ]:


# Add brick anomaly
# #############################################################################

M1.add_brick(start=[-700., 300., -500.],       # lower left back corner
             stop=[-500., 700., -1000.],      # upper right front corner
             cell_size=1e4)                      # max. cell volume constraint


# In[ ]:


# Add dipping plate anomaly
# #############################################################################

#M2.add_plate(500., 500., 50.,                  # length, width, height
#             [0.0, 0.0, -700.],             # origin (center) of plate
#             45., 117.,                        # dip, dip azimuth
#             cell_size=1e4)                    # max. cell volume constraint
#M3.add_plate(500., 500., 50.,                  # length, width, height
#             [0.0, 0.0, -700.],             # origin (center) of plate
#             45., 117.,                        # dip, dip azimuth
#             cell_size=1e4)                    # max. cell volume constraint


# ### Add tetrahedron-boundary
#
# - Used to increase the domain size for reducing boundary effects (low freq.)

# In[ ]:


# The computational domain size is increased in x-, y- z-dir. by the
# three given factors, respecively.
# #############################################################################

M1.extend_world(1e1, 1e1, 1e1)
# M2.there_is_always_a_bigger_world(1e2, 1e2, 1e2)
# M3.there_is_always_a_bigger_world(1e2, 1e2, 1e2)


# ### Call TetGen for meshing
#
# - Automatically export **Omega** to *.poly* file in **w_dir**
# - Call TetGen and export in *.vtk* for Paraview and *.mesh* (Medit format)
# - Automatically copy *.mesh* filed to **s_dir** for automated conversion

# In[ ]:


# Call TetGen with different command line options and set further arguments.
# for the TetGen options, it is referred to the TetGen documentation
# #############################################################################

M1.call_tetgen(tet_param='-pq1.2aA',
              # tet_param='default,             # '-pq1.2aA',
              # tet_param='raw',                # '-p'
              # tet_param='faces',              # '-pMA'
              # print_infos=True,               # print mesh infos at the end
              # export_before=True,             # export *.poly* file
              # # # # # # # # # # # # # # # # # # automatically
              # copy=True                       # copy to **s_dir**
              )

M2.call_tetgen(tet_param='-pDq1.2aA')
M3.call_tetgen(tet_param='-pDq1.2aA')

