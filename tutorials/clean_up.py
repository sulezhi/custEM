# -*- coding: utf-8 -*-
"""
@author:  Rochlitz.R
"""

import shutil


try:
    shutil.rmtree('meshes')
except FileNotFoundError:
    pass

try:
    shutil.rmtree('results')
except FileNotFoundError:
    pass

try:
    shutil.rmtree('plots')
except FileNotFoundError:
    pass
