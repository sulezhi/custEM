# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 15:14:20 2016

@author: Rochlitz.R
"""

import dolfin as df
from custEM.misc import mpi_print as mpp


class Plate(df.SubDomain):

    """
    Define plate anomaly with FEniCS syntax, deprecated - do not use.
    """

    def __init__(self, thick=50.0, origin=[0.0, 0.0, -200.0], l_x=200.0,
                 l_y=200.0, x_dip=0.0, y_dip=0.0, tol=1e-2, **df_kwargs):

        df.SubDomain.__init__(self)
        self.tol = tol
        self.origin = origin
        self.x_min = origin[0] - l_x/2.0 - tol
        self.x_max = origin[0] + l_x/2.0 + tol
        self.y_min = origin[1] - l_y/2.0 - tol
        self.y_max = origin[1] + l_y/2.0 + tol
        self.z_min = origin[2] - thick/2.0 - tol
        self.z_max = origin[2] + thick/2.0 + tol
        self.x_dip = x_dip
        self.y_dip = y_dip

        mpp('... using Plate anomaly')

    def inside(self, x, on_boundary):

        tol = self.tol
        top = x[0] * self.x_dip + x[1] * self.y_dip + self.z_max
        bottom = x[0] * self.x_dip + x[1] * self.y_dip + self.z_min
        return (x[2] > bottom - tol and x[2] < top + tol and
                x[1] > self.y_min - tol and x[1] < self.y_max + tol and
                x[0] > self.x_min - tol and x[0] < self.x_max + tol)
