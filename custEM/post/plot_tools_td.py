# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as clrs
from custEM.misc import pyhed_calculations as phc
from custEM.post import plot_utils as pu
from custEM.post.plot_utils import PlotBase
import custEM.misc
import sys
import os
from matplotlib import cm
from matplotlib.colors import LogNorm
from matplotlib import rcParams


class PlotTD(PlotBase):

    """
    Plot class for visualization of custEM results.

    !!! IN DEVELOPMENT !!!

    class internal functions
    ------------------------

    """

    def __init__(self, mod, mesh, approach, **plot_kwargs):

        """
        Submodule for importing, comparing and illustrating interpolated
        time-domain FE approach results in 1D, 2D and 3D. So far, lines and
        slices parallel or orthogonal to the coordinate-system axes are
        supported, even with topography. However, support for arbitrary
        directed interpolation paths or planes still needs to be implemented.

        Note:
        -----

        - Field quantities are referred to as **E**, **dBdt** or **B**.


        Required arguments:
        -------------------

        - mod_name, type str
            name of the model

        - mesh_name, type str
            name of input mesh without suffix

        - approach, type str
            either **E_IE**, **E_RA**, **E_FT**

        Keyword arguments:
        ------------------

        - r_dir = '../results/', type str
            specify custom modeling results directory if required.

        - s_dir = '.', type str
            specify custom save-directory for figures if required.

        - fig_size = None, type list with len = 2
            global adjustment of figure sizes [inch].

        - dpi = 300, type int
            dpi value for saving figures in "png" format

        - label_color = '#000000', type str
            string of hexahedral color code to define axis and label colors
            for plots

        - fs = 12, type int
            default font size for tick-labels, legends etc.

        - dg_space = None, type bool
            set *True* if data are loaded from discontinuous interpolation
            to automatically remove multiple values at the same positions and
            sort the data. Please note, this approach is experimental and
            should be used with caution
        """

        super(self.__class__, self).__init__()

        self.mod = mod
        self.mesh = mesh
        self.approach = approach
        self.r_dir = '../results/'
        self.s_dir = '../plots/'
        self.load_parameters = True

        for key in plot_kwargs:
            if key not in self.__dict__:
                print('!!! Warning: Unknown Plot class argument set:', key)
        self.__dict__.update(**plot_kwargs)

        rcParams['axes.edgecolor'] = self.label_color
        rcParams['xtick.color'] = self.label_color
        rcParams['ytick.color'] = self.label_color
        rcParams['axes.labelcolor'] = self.label_color
        rcParams['text.color'] = self.label_color
        rcParams['axes.unicode_minus'] = False

        if not os.path.isdir(self.s_dir):
            os.makedirs(self.s_dir)
        rcParams['axes.unicode_minus'] = False
        if self.fig_size is not None:
            rcParams['figure.figsize'] = self.fig_size[0], self.fig_size[1]

    def import_line_data(self, linE, mod=None, mesh=None, approach=None,
                         EH='EHdH', shut_off=True, shut_on=False, sf=None,
                         path=None, key=None, stride=1, tx=None, times=None):

        mod, mesh, approach, path = self.init_main_parameters(
                                            mod, mesh, approach, path)
        comp = self.init_component_integers(linE[-1])

        if tx is not None:
            path += 'tx_' + str(tx) + '_'

        if times is None:
            if self.times is None:
                print('Error! Specify the import *times* either as a class '
                      'attribute during initialization or as keyword argument '
                      'during import. Aborting  ...')
                raise SystemExit
            else:
                times = self.times

        if shut_off:
            if 'E' in EH:

                if key is None:
                    data_key = mod + '_E_off_on_' + linE
                else:
                    data_key = key + '_E_off'

                data_s = []
                for jj in range(len(fem_times)):
                    try:
                        data = np.load(path + 'E_off_on_' + linE + '.npy')
                    except FileNotFoundError:
                        self.print_import_error('E_off_on_', linE, path)
                    else:
                        data_s.append(self.arrange_line_data(
                            data, comp, stride))

                self.line_data.update({data_key: np.array(data_s)[:, :, :3]})
                self.line_coords.update({linE: data_s[0, :, 3:]})

            if 'H' in EH.replace('dH', ''):

                if key is None:
                    data_key = mod + '_H_off_on_' + linE
                else:
                    data_key = key + '_H_off'

                data_s = []
                for jj in range(len(fem_times)):
                    try:
                        data = np.load(path + 'H_off_on_' + linE + '.npy')
                    except FileNotFoundError:
                        self.print_import_error('H_off_on_', linE, path)
                    else:
                        data_s.append(self.arrange_line_data(
                            data, comp, stride))

                self.line_data.update({data_key: np.array(data_s)[:, :, :3]})
                self.line_coords.update({linE: data_s[0, :, 3:]})

        if shut_on:
            if 'E' in EH:

                if key is None:
                    data_key = mod + '_E_on_on_' + linE
                else:
                    data_key = key + '_E_on'

                data_s = []
                for jj in range(len(fem_times)):
                    try:
                        data = np.load(path + 'E_on_on_' + linE + '.npy')
                    except FileNotFoundError:
                        self.print_import_error('E_on_on_', linE, path)
                    else:
                        data_s.append(self.arrange_line_data(
                            data, comp, stride))

                self.line_data.update({data_key: np.array(data_s)[:, :, :3]})
                self.line_coords.update({linE: data_s[0, :, 3:]})

            if 'H' in EH.replace('dH', ''):

                if key is None:
                    data_key = mod + '_H_on_on_' + linE
                else:
                    data_key = key + '_H_on'

                data_s = []
                for jj in range(len(fem_times)):
                    try:
                        data = np.load(path + 'H_on_on_' + linE + '.npy')
                    except FileNotFoundError:
                        self.print_import_error('H_on_on_', linE, path)
                    else:
                        data_s.append(self.arrange_line_data(
                            data, comp, stride))

                self.line_data.update({data_key: np.array(data_s)[:, :, :3]})
                self.line_coords.update({linE: data_s[0, :, 3:]})

        if 'd' in EH:
            if key is None:
                data_key = mod + '_dH_on_' + linE
            else:
                data_key = key + '_dH'

            data_s = []
            for jj in range(len(fem_times)):
                try:
                    data = np.load(path + 'dH_on_' + linE + '.npy')
                except FileNotFoundError:
                    self.print_import_error('dH_on_', linE, path)
                else:
                    data_s.append(self.arrange_line_data(
                        data, comp, stride))

            self.line_data.update({data_key: np.array(data_s)[:, :, :3]})
            self.line_coords.update({linE: data_s[0, :, 3:]})
